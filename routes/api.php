<?php

use App\Http\Controllers\BotController;
use App\Http\Controllers\DonateController;
use App\Http\Middleware\TokenCheckMiddleware;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Telegram\Bot\Laravel\Facades\Telegram;
use App\Http\Controllers\TelegramAuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['auth:sanctum'])->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/bot',[BotController::class,'show']);

Route::post('/login',[TelegramAuthController::class,'handleTelegramAuth']);
Route::post('/logout',[TelegramAuthController::class,'handleTelegramLogout']);


Route::get('donators',[DonateController::class,'showDonators']);
Route::get('random_donate',[DonateController::class,'getItem']);

//Route::middleware('token.check')->group(function(){
    Route::post('addDonate',[DonateController::class,'addDonate']);
    Route::get('checkDonate',[DonateController::class,'checkDonate']);
	Route::get('available',[DonateController::class,'getPaymentForm']);
//});




