<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Queue;
use App\Jobs\CreditToAcctount;

class StartTransactionQueue extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:start-transaction-queue';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Запуск очередей для транзакций';

    /**
     * Execute the console command.
     */
    public function handle()
    {
	    $this->info('Очередь запущена!');
	    exec('php artisan queue:work --daemon > /dev/null 2>&1 &');
    }
}
