<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserBot extends Model
{
    use HasFactory;


	protected $connection = 'fishbotbase';

	protected $table = 'userBot';

	protected $guarded = ['id'];

	public $timestamps = false;

}
