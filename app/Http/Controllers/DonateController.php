<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserDonate;
use App\Models\FishBattleSaiteUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Services\MerchantService;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;
use App\Jobs\CreditToAcctount;

class DonateController extends Controller
{
	public function showDonators()
	{
		$donate = User::with('donate')->get();
		return response()->json($donate);
	}

	public function __construct(
		protected MerchantService $merchantService
	)
	{
	}

	public function addDonate(Request $request)
	{
		$request->validate([
			'amount' => ['required','min:200','numeric'],
		    'tg_id'=>Rule::exists('users','tg_id'),
		    'method'=>['required',Rule::in(MerchantService::arrayPayForm())]
		]);

//		if (Auth::check()){

			try {
				$addDonate = MerchantService::createTransaction(
					amount: $request['amount'],
					userId: $request['tg_id'],
					method: $request['method']
				);

				$transactionStatus = MerchantService::getTransactionInfo(
					$addDonate->id
				);

				CreditToAcctount::dispatch(
					transId: $addDonate->id,
					tgId:$request['tg_id']
				);

				return [
					'payment_url' => $transactionStatus,
				];

			} catch (\Exception) {
				Log::info('Проблема при создании и получении реквизитов');
			}
//	    }
	}

    public function getPaymentForm(Request $request)
    {
    	$availableMethods = MerchantService::getAvailablePayment();

    	$method = [];

    	foreach ($availableMethods[0]->methods as $item) {
    		$method[$item->method] = $item->name;
	    }

    	return $method;

    }

    public function checkDonate()
    {
    	$checkTransactionStatus = MerchantService::getTransactionInfo('c4d39199-34b2-49a5-89e7-0b46b1476217');

    	return $checkTransactionStatus;
    }
}
