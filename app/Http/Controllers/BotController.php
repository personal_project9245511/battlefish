<?php

namespace App\Http\Controllers;

use App\Services\BotService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Telegram\Bot\Api;
use Telegram\Bot\Laravel\Facades\Telegram;

class BotController extends Controller
{
    protected $telegram;
    protected $chatId;

    public function __construct(Api $telegram)
    {
        $this->telegram = $telegram;
        $this->chatId = BotService::getChatId();
    }

    public function show()
    {
        return Telegram::getMe();
    }

}
