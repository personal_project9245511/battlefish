<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Services\BotService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Str;

class TelegramAuthController extends Controller
{
    public function handleTelegramAuth(Request $request)
    {
        try {
            $user = User::where('tg_id', $request['user_id'])->first();

            if (!$user) {
                $user = User::create([
                    'name' => $request['first_name'],
                    'username' => $request['user_name'],
                    'tg_id' => $request['user_id'],
                ]);

                $user->remember_token = $user->createToken('authToken', ['*'])->plainTextToken;
                $user->save();
            } elseif (empty($user->remember_token)) {
	            $user->remember_token = $user->createToken('authToken', ['*'])->plainTextToken;
                $user->save();
            }

            $response = response()->json(['token'=>$user->remember_token]);

            $response->cookie(
            	'tt',$user->remember_token,120
            );

            return $response;

        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }

    }

    public function handleTelegramLogout(Request $request) {
        $bearerToken = $request->header('Authorization');

        if (strpos($bearerToken, 'Bearer ') === 0) {
            $token = substr($bearerToken, 7);

            $user = User::where('remember_token', $token)->firstOrFail();
            if ($user) {
                $user->update(['remember_token' => null]);
                return response()->json(['message'=>'Token deleted for user: ' . $user->id]);
            }
        }

    }
}
