<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class TokenCheckMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle($request, Closure $next)
    {
        $bearerToken = $request->header('Authorization');

        if (strpos($bearerToken, 'Bearer ') === 0) {
            $token = substr($bearerToken, 7);

            $user = User::where('remember_token', $token)->first();

            if ($user) {
                Auth::login($user);
            }
        }

        return $next($request);
    }

}
