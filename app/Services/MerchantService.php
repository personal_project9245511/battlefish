<?php

namespace App\Services;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use App\Models\TransactionUser;


class MerchantService
{

	protected static $token = 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiIzQ1p3Nk5pem5QY2FtVDk5a0E4N21obVh4RnAyIiwiZGF0ZSI6IjIwMjMtMTItMDFUMTE6MDU6MzEuNTgzWiIsImlhdCI6MTcwMTQyODczMX0.H-UN5QcQLoonEKmFGLgZrWt0h7GP5_GcPeI8tuxo4HM';

	static function getValidToken()
	{
		$req = Http::withHeaders([
			'Authorization' => self::$token,
			'Content-Type' => "application/x-www-form-urlencoded"
		])
			->post('https://api.merchant001.io/v1/healthcheck/merchant');

		$status = json_decode($req->body());

		if ($status->success) {
			return ['status', $status->success, 'token' => self::$token];
		} else {
			return ['status' => 'Ошибка валидации токена'];
		}

	}


	static function getAvailablePayment()
	{
		$req = Http::withHeaders([
			'Authorization' => self::getValidToken()['token'],
			"Content-Type" => "application/x-www-form-urlencoded"
		])
			->get('https://api.merchant001.io/v1/payment-method/merchant/available?makeArray=1');

		if ($req->status() == 200) {
			return json_decode($req->body());
		}
	}

	static function getAvailablePaymentForAmount(int $amount)
	{
		$req = Http::withHeaders([
			'Authorization' => self::getValidToken()['token'],
			"Content-Type" => "application/x-www-form-urlencoded"
		])
			->get('https://api.merchant001.io/v1/payment-method/merchant/available?makeArray=1&onlyMethod=1&amount=' . $amount);

		if ($req->status() == 200) {
			return json_decode($req->body());
		}

		if ($req->status() == 400) {
			return ['status' => 'Unavailable methods for your amount'];
		}

		return json_decode($req->body());
	}

	static function createTransaction($amount,$userId,$method)
	{
		$amountMethod = self::getAvailablePaymentForAmount($amount);
		$transKey = self::createTransKey($userId);

		$req = Http::withHeaders([
			'Authorization' => self::getValidToken()['token']
		])
			->post('https://api.merchant001.io/v1/transaction/merchant', [
				'isPartnerFee' => true,
				'pricing' => [
					'local' => [
						'amount' => $amount,
						'currency' => 'RUB'
					]
				],
				'selectedProvider' => [
					'type' => $amountMethod[0]->type,
					'method' => $method
				],
				'invoiceId'=>$transKey
			]);

		if ($req->status() !== 200) {
			Log::error('Метод createTransaction' . $req->status() . $req->body());
		};

		$response = json_decode($req->body());

		$userTrans = TransactionUser::create([
			'trans_key'=>$transKey,
			'uuid'=>$response->id,
			'status'=>$response->status,
			'tg_id'=>$userId
		]);

		$userTrans->save();

		return json_decode($req->body());
	}

	public static function getTransactionInfo($id)
	{
		$req = Http::withHeaders([
			'Authorization' => self::getValidToken()['token']
		])
			->get('https://api.merchant001.io/v1/transaction/merchant/'.$id);

		if ($req->status() !== 200) {
			Log::error('Метод getTransactionInfo' . $req->status() . $req->body());
		}

		 return json_decode($req->body());
	}

	public static function getTransactionRequisites($tansId)
	{
		$req = Http::withHeaders([
			'Authorization' => self::getValidToken()['token']
		])
			->get('https://api.merchant001.io/v1/transaction/merchant/requisite/'.$tansId);

		if($req->status() !== 200) {
			Log::error('Метод getTransactionRequisites' . $req->status() . $req->body());
		}

		return json_decode($req->body());
	}

	public static function arrayPayForm()
	{
		$availableMethods = self::getAvailablePayment();

		$method = [];

		foreach ($availableMethods[0]->methods as $item) {
			$method[] = $item->method;
		}

		return $method;
	}


	protected static function createTransKey($userId,$length = 16)
	{
		return $userId . Str::random($length);
	}
}
