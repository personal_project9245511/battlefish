<?php

namespace App\Services;

use GuzzleHttp\Client;
use Telegram\Bot\Laravel\Facades\Telegram;
use TgWebValid\TgWebValid;

class BotService
{
    static function getChatId()
    {
        $client = new Client();

        $response = $client->request('GET','https://api.telegram.org/bot'.env('TELEGRAM_BOT_TOKEN').'/getUpdates');

        $statusCode = $response->getStatusCode();
        $body = $response->getBody()->getContents();

        $data = json_decode($body, true);

        $chatId = $data['result'][0]['message']['chat']['id'];

        return (string)$chatId;
    }


    public function sendMessageToBot($request)
    {
        if($request['auth']){
            Telegram::sendMessage([
                'chat_id' => '250640154',
                'text' => 'User: ' . $request['name'] . ' Auth'
            ]);
        }

        if($request['create']) {
            Telegram::sendMessage([
                'chat_id' => '250640154',
                'text' => 'User: ' . $request['name'] . ' Create'
            ]);
        }

    }

    public function getBotObject()
    {
         $tgWebValid = new TgWebValid(env('TELEGRAM_BOT_TOKEN'), false);
         $bot = $tgWebValid->bot();

         return $bot;
    }
}
