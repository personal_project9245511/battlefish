<?php

namespace App\Jobs;

use App\Models\TransactionUser;
use App\Models\User;
use App\Models\UserBot;
use App\Services\MerchantService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class CreditToAcctount implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $transId;
    protected $tgId;
    /**
     * Create a new job instance.
     */
    public function __construct($transId,$tgId)
    {
        $this->transId = $transId;
        $this->tgId = $tgId;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
    	info('Джоба запущена');

		try {
			$getTransReq = MerchantService::getTransactionInfo($this->transId);
			$userTrans = TransactionUser::where('uuid',$this->transId)->first();
			$fishBotTable = UserBot::where('id_chat',$userTrans->tg_id)->first();

			info($getTransReq->status . ' статус');

			if($getTransReq->status === 'PENDING' ||$getTransReq->status === 'CREATED') {
				$userTrans->status =  $getTransReq->status  == 'PENDING' ? 'PENDING' : 'CREATED';
				$userTrans->update_attempts =  $userTrans->update_attempts + 1;
				$userTrans->save();

				Log::info('Повтор запроса ' . $this->transId . Carbon::now());
				CreditToAcctount::dispatch($this->transId,$userTrans->tg_id)->delay(now()->addMinute());
			}

			if($getTransReq->status === 'EXPIRED') {
				$userTrans->status = 'EXPIRED';
				$userTrans->save();

				Log::info('Время на оплату истекло ' . $this->transId . Carbon::now());
			}

			if($getTransReq->status === 'CANCELED') {
				$userTrans->status = 'CANCELED';
				$userTrans->save();

				Log::info('Отменено ' . $this->transId . Carbon::now());
			}

			if($getTransReq->status === 'PAID') {
				$userTrans->status = 'PAID';
				$userTrans->save();

				if ($getTransReq->transaction && $getTransReq->transaction->pricing && $getTransReq->transaction->pricing->local) {
					$fishBotTable->check = $getTransReq->transaction->pricing->local->amount;
					$fishBotTable->save();
					Log::info('Оплачено ' . $this->transId . Carbon::now());
				} else {
					Log::info('Ошибка получения информации о транзакции ' . $this->transId . Carbon::now());
				}
			}

		}catch (\Exception $exception){
			Log::info('CreditToAccountJob '. $exception->getMessage());
		}
    }
}
