<?php

namespace App\Jobs;

use GuzzleHttp\Client;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\PendingDispatch;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use App\Models\TransactionUser;
use App\Services\MerchantService;
use App\Models\Podcast;
use App\Services\AudioProcessor;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;


class CheckTransactionStatus implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	protected $transactionId;


	public function __construct($transactionId)
	{
		$this->transactionId = $transactionId;
	}

	public function handle()
	{
		$logMessage = 'Скрипт был запущен: ' . now();
		Log::info($logMessage);

		try {
			$req = Http::withHeaders([
				'Authorization' => MerchantService::getValidToken()['token']
			])
				->get('https://api.merchant001.io/v1/transaction/merchant/'.$this->transactionId);

			if ($req->status() !== 200) {
				Log::error('Метод handle/job' . $req->status() . $req->body());
			}

			if ($req->status() == 200) {
				$response = json_decode($req->body());
				if($response->status == 'PENDING') {
					info(MerchantService::getTransactionRequisites($this->transactionId));
				}
			}
		} catch (\Exception $exception){
			Log::error('Метод handle/job' . $exception->getMessage());
		}
	}
}
